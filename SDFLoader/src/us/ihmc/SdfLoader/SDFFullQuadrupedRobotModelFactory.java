package us.ihmc.SdfLoader;

public interface SDFFullQuadrupedRobotModelFactory extends SDFFullRobotModelFactory
{
   SDFFullQuadrupedRobotModel createFullRobotModel();
}
