package us.ihmc.robotics.generatedTestSuites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite.SuiteClasses;

import us.ihmc.tools.testing.TestPlanSuite;
import us.ihmc.tools.testing.TestPlanSuite.TestSuiteTarget;
import us.ihmc.tools.testing.TestPlanTarget;

/** WARNING: AUTO-GENERATED FILE. DO NOT MAKE MANUAL CHANGES TO THIS FILE. **/
@RunWith(TestPlanSuite.class)
@TestSuiteTarget(TestPlanTarget.Fast)
@SuiteClasses
({
   us.ihmc.robotics.math.corruptors.NoisyYoRotationMatrixTest.class,
   us.ihmc.robotics.math.filters.AlphaBetaFilteredYoVariableTest.class,
   us.ihmc.robotics.math.filters.AlphaFilteredWrappingYoVariableTest.class,
   us.ihmc.robotics.math.filters.AlphaFilteredYoFrameQuaternionTest.class,
   us.ihmc.robotics.math.filters.AlphaFilteredYoVariableTest.class,
   us.ihmc.robotics.math.filters.AlphaFusedYoVariableTest.class,
   us.ihmc.robotics.math.filters.BacklashCompensatingVelocityYoVariableTest.class,
   us.ihmc.robotics.math.filters.BacklashProcessingYoVariableTest.class,
   us.ihmc.robotics.math.filters.BetaFilteredYoVariableTest.class,
   us.ihmc.robotics.math.filters.DeadzoneYoVariableTest.class,
   us.ihmc.robotics.math.filters.DelayedBooleanYoVariableTest.class,
   us.ihmc.robotics.math.filters.DelayedDoubleYoVariableTest.class,
   us.ihmc.robotics.math.filters.DeltaLimitedYoVariableTest.class,
   us.ihmc.robotics.math.filters.FilteredDiscreteVelocityYoVariableTest.class,
   us.ihmc.robotics.math.filters.FilteredVelocityYoVariableTest.class,
   us.ihmc.robotics.math.filters.FirstOrderFilteredYoVariableTest.class,
   us.ihmc.robotics.math.filters.GlitchFilteredBooleanYoVariableTest.class,
   us.ihmc.robotics.math.filters.HysteresisFilteredYoVariableTest.class,
   us.ihmc.robotics.math.filters.RateLimitedYoVariableTest.class,
   us.ihmc.robotics.math.filters.SimpleMovingAverageFilteredYoVariableTest.class,
   us.ihmc.robotics.math.frames.YoFramePointInMultipleFramesTest.class,
   us.ihmc.robotics.math.frames.YoFrameQuaternionTest.class,
   us.ihmc.robotics.math.frames.YoMatrixTest.class,
   us.ihmc.robotics.math.frames.YoMultipleFramesHelperTest.class,
   us.ihmc.robotics.math.interpolators.OrientationInterpolationCalculatorTest.class,
   us.ihmc.robotics.math.interpolators.QuinticSplineInterpolatorTest.class,
   us.ihmc.robotics.math.MatrixYoVariableConversionToolsTest.class,
   us.ihmc.robotics.math.QuaternionCalculusTest.class,
   us.ihmc.robotics.math.TimestampedVelocityYoVariableTest.class,
   us.ihmc.robotics.math.trajectories.CirclePositionTrajectoryGeneratorTest.class,
   us.ihmc.robotics.math.trajectories.ConstantAccelerationTrajectoryGeneratorTest.class,
   us.ihmc.robotics.math.trajectories.ConstantForceTrajectoryGeneratorTest.class,
   us.ihmc.robotics.math.trajectories.ConstantOrientationTrajectoryGeneratorTest.class,
   us.ihmc.robotics.math.trajectories.ConstantPoseTrajectoryGeneratorTest.class,
   us.ihmc.robotics.math.trajectories.ConstantPositionTrajectoryGeneratorTest.class,
   us.ihmc.robotics.math.trajectories.ConstantVelocityTrajectoryGeneratorTest.class,
   us.ihmc.robotics.math.trajectories.HermiteCurveBasedOrientationTrajectoryGeneratorTest.class,
   us.ihmc.robotics.math.trajectories.IntermediateWaypointVelocityGeneratorTest.class,
   us.ihmc.robotics.math.trajectories.OneDoFJointQuinticTrajectoryGeneratorTest.class,
   us.ihmc.robotics.math.trajectories.OrientationInterpolationTrajectoryGeneratorTest.class,
   us.ihmc.robotics.math.trajectories.PositionTrajectorySmootherTest.class,
   us.ihmc.robotics.math.trajectories.ProviderBasedConstantOrientationTrajectoryGeneratorTest.class,
   us.ihmc.robotics.math.trajectories.providers.YoOrientationProviderTest.class,
   us.ihmc.robotics.math.trajectories.providers.YoPositionProviderTest.class,
   us.ihmc.robotics.math.trajectories.providers.YoSE3ConfigurationProviderTest.class,
   us.ihmc.robotics.math.trajectories.providers.YoVariableDoubleProviderTest.class,
   us.ihmc.robotics.math.trajectories.SimpleOrientationTrajectoryGeneratorTest.class,
   us.ihmc.robotics.math.trajectories.StraightLinePositionTrajectoryGeneratorTest.class,
   us.ihmc.robotics.math.trajectories.VelocityConstrainedOrientationTrajectoryGeneratorTest.class,
   us.ihmc.robotics.math.trajectories.waypoints.FrameEuclideanTrajectoryPointTest.class,
   us.ihmc.robotics.math.trajectories.waypoints.FrameSE3TrajectoryPointTest.class,
   us.ihmc.robotics.math.trajectories.waypoints.FrameSO3TrajectoryPointTest.class,
   us.ihmc.robotics.math.trajectories.waypoints.MultipleWaypointsOrientationTrajectoryGeneratorTest.class,
   us.ihmc.robotics.math.trajectories.waypoints.MultipleWaypointsPositionTrajectoryGeneratorTest.class,
   us.ihmc.robotics.math.trajectories.waypoints.MultipleWaypointsTrajectoryGeneratorTest.class
})

public class IHMCRoboticsToolkitBFastTestSuite
{
   public static void main(String[] args)
   {

   }
}
