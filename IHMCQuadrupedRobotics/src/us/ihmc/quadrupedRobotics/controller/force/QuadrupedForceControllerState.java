package us.ihmc.quadrupedRobotics.controller.force;

public enum QuadrupedForceControllerState
{
   JOINT_INITIALIZATION,
   STAND_PREP,
   STAND_READY,
   STAND,
   STEP,
   TROT,
   PACE,
   XGAIT
}