package us.ihmc.quadrupedRobotics.controller.force;

public enum QuadrupedForceControllerRequestedEvent
{
   REQUEST_STAND_PREP,
   REQUEST_STAND,
   REQUEST_STEP,
   REQUEST_PACE,
   REQUEST_TROT,
   REQUEST_XGAIT
}
