package us.ihmc.quadrupedRobotics.controller.forcedev;

public enum QuadrupedForceDevelopmentControllerState
{
   JOINT_INITIALIZATION,
   STAND_PREP,
   STAND_READY,
   TROT_WALK,
}