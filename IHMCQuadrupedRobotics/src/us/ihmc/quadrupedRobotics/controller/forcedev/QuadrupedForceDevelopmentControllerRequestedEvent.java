package us.ihmc.quadrupedRobotics.controller.forcedev;

public enum QuadrupedForceDevelopmentControllerRequestedEvent
{
   REQUEST_STAND_PREP,
   REQUEST_TROT_WALK,
}
