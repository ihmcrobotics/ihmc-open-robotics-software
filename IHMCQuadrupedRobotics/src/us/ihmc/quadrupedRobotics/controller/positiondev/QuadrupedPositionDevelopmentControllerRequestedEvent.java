package us.ihmc.quadrupedRobotics.controller.positiondev;

public enum QuadrupedPositionDevelopmentControllerRequestedEvent
{
   REQUEST_STAND_PREP,
   REQUEST_COM_VERIFICATION,
   REQUEST_JOINT_SLIDER_BOARD,
}
