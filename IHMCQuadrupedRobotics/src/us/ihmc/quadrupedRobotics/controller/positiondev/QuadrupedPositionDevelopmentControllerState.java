package us.ihmc.quadrupedRobotics.controller.positiondev;

public enum QuadrupedPositionDevelopmentControllerState
{
   JOINT_INITIALIZATION,
   DO_NOTHING,
   STAND_PREP,
   STAND_READY,
   COM_VERIFICATION,
   JOINT_SLIDER_BOARD
}